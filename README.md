# Atb


### Obraz bazowy

https://hub.docker.com/r/nginxinc/nginx-unprivileged/  
https://hub.docker.com/_/nginx

### Budowa

```shell
docker login --username=kornelbrenkacz
docker build . -t kornelbrenkacz/atb:1.0
docker push kornelbrenkacz/atb:1.0
oc tag --source=docker kornelbrenkacz/atb:3.0 sandbox/atb:3.0
```

### Linki
