(function () {
    'use strict';
    angular
        .module('atb-extension', ['openshiftConsole'])
        .run([
            'extensionRegistry',
            function (extensionRegistry) {
                extensionRegistry
                    .add('nav-help-dropdown', function () {
                        return [
                            {
                                type: 'dom',
                                node: '<li class="divider"></li>'
                            }, {
                                type: 'dom',
                                node: '<li><a href="https://livetech.asseco.com/techbreakfast/TBF-2022-05-12" target="_blank">Kolejny TechBreakfast 12 maja</a></li>'
                            }, {
                                type: 'dom',
                                node: '<li><a href="https://livetech.asseco.com/techbreakfast" target="_blank">TechBreakfast Home Page</a></li>'
                            }
                        ];
                    });
            }
        ]);

    hawtioPluginLoader.addModule('atb-extension');

    angular
        .module('atb-status', ['openshiftConsole'])
        .run([
            'extensionRegistry',
            function (extensionRegistry) {
                var system_status_elem = $('<a href="https://livetech.asseco.com/techbreakfast/"' +
                    'target="_blank" class="nav-item-iconic system-status"><span title="' +
                    'Houston mamy problem!!!" class="fa-solid fa-mug">' +
                    '</span></a>');

                extensionRegistry
                    .add('atb-status', function () {
                        return [{
                            type: 'dom',
                            node: system_status_elem
                        }];
                    });
            }
        ]);

    hawtioPluginLoader.addModule('atb-status');


    window.OPENSHIFT_CONSTANTS.PROJECT_NAVIGATION.push(
        {
            label: "Tech Breakfast ☕",
            iconClass: "fa fa-user-secret",
            href: "https://livetech.asseco.com/techbreakfast/TBF-2022-05-11"
        },
        {
            label: "Infra",
            iconClass: "fa fa-cubes",
            secondaryNavSections: [
                {
                    items: [
                        {
                            label: "Jenkins",
                            href: "https://www.jenkins.io/"
                        }
                    ]
                },
                {
                    header: "Gitlab",
                    items: [
                        {
                            label: "Gitlab",
                            href: "https://gitlab.com"
                        }
                    ]
                }
            ]
        }
    );


}());

